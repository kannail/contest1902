# Un MCD ou un MLD de la base de données correspondant à la demande exprimée
## Analyse
- Tous ces produits et services ont en commun un libellé, une description courte, une notice d'usage et un prix.

 - Les produits et service sont relativement similaires et vont donc être compris dans la même table.

 - La seule différence notable trouver et le temps de garanti, tous les objets n'ayant pas forcément une garantis, j'ai jugé que dans notre cas un garanti pouvais être null afin de conserver une seule table pour les deux éléments.


- Des produits caractérisés par un volume, un nombre de colis et une période de garantie.

- Des services caractérisés par un délai de prise en compte de la demande, une information indiquant si le service est ponctuel ou sous forme d'abonnement.

 - Ensuite pour les caractéristiques, elles viendront se greffe lors de l'achat de la personne dans une table d'association.

- Un utilisateur souscrit à une ou plusieurs offres.
 - Un agent lui attribue une agence principale.

- Les agents de l'entreprise sont rattachés à une agence et peuvent être référents sur un produit ou service du catalogue, sans que ce soit obligatoire. Ils ont une date d'entrée dans l'entreprise.

![](Utilisateur.PNG)

## Idée
On part d'une table personne, elles peuvent être soit des salariés soit des consommateurs, de plus un agent peut être ou non dans une agence.

De plus, les consommateurs vont leur voir attribuer une agence lors de l'achat d'un service.

Ensuite un consommateur peut acheter un ou plusieurs produit, un ou plusieurs services, qui seront tous les deux compris dans la table produite services.

La table ProduitsServices prend aussi en charge le temps de garantir des different produits.

Un salarié peut être referents sur un produit.

Une agence peut être attribué à un salarié et un salarié va pouvoir attribuer une agence à un consommateur lors de l'achat d'un service

## Realisation

![](MCDSQLDBM.PNG)


## Explication

```
Realisation :
Agence -> Personnes {
  - Une agence peut être attribué à un salarié et
    un salarié va pouvoir attribuer une agence à
    un consommateur lors de l'achat d'un service.
}

Personnes ->  Consomateurs
              Salariés{
  - Une personne peut être soit Consommateurs soit Salariés.
}

Salariés -> ProduitsServices {
  - Un salarié va pouvoir être referents d'un produit.
}

Consomateurs -> UsersService
                UsersProducts {
  - Un consommateur va pouvoir acheter plusieurs Service
    et produit afin de sécuriser sa maison comme jamais.
}

UsersServices
UsersProducts -> ProduitsServices {
  - Lors de l'achat d'un produit, en fonction des attributs
    donné, cela sera soit un Services soit un produit
}

```
